import React, { Component } from "react";
import { PureComponent } from "react";

// PureComponent hạn chế render không cần thiết
// export default class Header extends PureComponent {
export default class Header extends Component {
  constructor() {}

  componentDidMount() {
    let count = 0;
    this.myInterval = setInterval(() => {
      count++;
      // console.log(count);
    }, 1000);
  }
  // PureComponent shallow commpare ~ chỉ có tác dụng với props là primitive value

  shouldComponentUpdate(nextProps, nextState) {
    console.log("nextState: ", nextState);
    console.log("nextProps: ", nextProps);
    if (nextProps.like == 3) {
      return false;
    } else {
      return true;
    }
  }
  render() {
    console.log("Header render");
    return (
      <div className=" display-4  py-5 bg-warning">
        <p>Header</p>
        {/* start like */}
        <div>Like: {this.props.like}</div>
        <button onClick={this.props.handlePlusLike} className="btn btn-success">
          Plus like
        </button>
        {/* end like */}
      </div>
    );
  }
  componentDidUpdate() {
    console.log("componentDidUpdate");
  }
  componentWillUnmount() {
    // componentWillUnmount sẽ tự động chạy khi component dc remove khỏi giao diện
    clearInterval(this.myInterval);
    console.log("will unmount - header");
  }
}
