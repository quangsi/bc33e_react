import React, { Component } from "react";
import { createRef } from "react";
import Header from "./Header";

export default class Demo_LifeCycle extends Component {
  constructor(props) {
    super(props);
    this.inputRef = createRef();
  }

  // hạn chế số lần render ko cần thiết , chèn 1 số đoạn code muốn thực thi trong từ giai đoạn trong lifecycle
  componentDidMount() {
    // componentDidMount ~ Chỉ chạy 1 lần duy nhất sau khi render() chạy lần đầu
    console.log("first render - componentDidMount");
    // gọi api

    console.log("this.inputRef.current", this.inputRef.current);

    this.inputRef.current.focus();
    // this.inputRef.current.style.background = "red";
  }

  state = {
    like: 0,
    share: 0,
  };
  handlePlusLike = () => {
    this.setState({ like: this.state.like + 1 });
  };
  handlePlusShare = () => {
    this.setState({
      share: this.state.share + 1,
    });
  };
  render() {
    console.log(" Parent render ");

    return (
      <div className="">
        {/*  */}
        <input type="text" ref={this.inputRef} />
        {this.state.like < 5 && (
          <Header handlePlusLike={this.handlePlusLike} like={this.state.like} />
        )}
        {/* start like */}
        <div className="">
          <span className="text-success display-4">{this.state.like}</span>
          <button onClick={this.handlePlusLike} className="btn btn-success">
            Plus like
          </button>
        </div>
        {/* end like */}

        {/* start share */}
        <div className="">
          <span className="text-danger display-4">{this.state.share}</span>
          <button onClick={this.handlePlusShare} className="btn btn-danger">
            Plus share
          </button>
        </div>
        {/* end share */}
      </div>
    );
  }
}

// hook
