import { combineReducers } from "redux";
import { shoeReducer } from "./shoeReducer";

export const rootReducer_Ex_Shoes_Redux = combineReducers({ shoeReducer });
