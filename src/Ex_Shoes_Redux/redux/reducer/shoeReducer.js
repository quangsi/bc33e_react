import { data_shoes } from "../../data_shoes";
import { ADD_TO_CART } from "../constants/shoeConstant";

let initialState = {
  shoes: data_shoes,
  gioHang: [],
};

export let shoeReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ADD_TO_CART: {
      // kiểm tra giỏ hàng hiện tại có sản phẩm hay chưa
      let index = state.gioHang.findIndex((item) => {
        return item.id == payload.id;
      });

      let cloneGioHang = [...state.gioHang];

      if (index == -1) {
        let newSp = { ...payload, soLuong: 1 };
        cloneGioHang.push(newSp);
      } else {
        cloneGioHang[index].soLuong++;
      }

      state.gioHang = cloneGioHang;
      return { ...state };
    }
    default:
      return state;
  }
};
