import React, { Component } from "react";
import { connect } from "react-redux";

class GioHang extends Component {
  renderTbody = () => {
    return this.props.cart.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price * item.soLuong}</td>
          <td>
            <img style={{ width: 100 }} src={item.image} alt="" />
          </td>
          <td>
            <button
              onClick={() => {
                this.props.handleChangeQuantity(item.id, 1);
              }}
              className="btn btn-success"
            >
              +
            </button>
            <span className="px-5">{item.soLuong}</span>
            <button
              onClick={() => {
                this.props.handleChangeQuantity(item.id, -1);
              }}
              className="btn btn-danger"
            >
              -
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div>
        <table className="table">
          <thead>
            <tr>
              <th>Id</th>
              <th>Tên</th>
              <th>Giá</th>
              <th>Hình ảnh</th>
              <th>Số lượng</th>
            </tr>
          </thead>
          <tbody>{this.renderTbody()}</tbody>
        </table>
      </div>
    );
  }
}
let mapStateToProps = (state) => {
  return {
    cart: state.shoeReducer.gioHang,
  };
};
export default connect(mapStateToProps)(GioHang);

// java, .net

// nodejs  express

// js
