import React, { Component } from "react";
import { dataPhone } from "./data";
import DetailPhone from "./DetailPhone";
import ListPhone from "./ListPhone";

// import * as data from "./dataPhone";
// console.log("data: ", data);

export default class Ex_Phone extends Component {
  state = {
    phones: dataPhone,
    detailPhone: dataPhone[0],
  };

  handleChangeDetail = (phone) => {
    this.setState({
      detailPhone: phone,
    });
  };
  render() {
    return (
      <div className="container py-5">
        <ListPhone
          hanleClick={this.handleChangeDetail}
          data={this.state.phones}
        />
        <DetailPhone detail={this.state.detailPhone} />
      </div>
    );
  }
}
