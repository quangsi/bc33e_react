import React, { Component } from "react";
import bgGAme from "../assets/bgGame.png";
import "./game.css";
import KetQua from "./KetQua";
import XucXac from "./XucXac";
export default class Ex_Tai_Xiu extends Component {
  render() {
    return (
      <div
        style={{
          backgroundImage: `url(${bgGAme})`,
          width: "100vw",
          height: "100vh",
        }}
        className="bg_game"
      >
        <XucXac />
        <KetQua />
      </div>
    );
  }
}
