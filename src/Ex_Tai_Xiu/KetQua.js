import React, { Component } from "react";
import { connect } from "react-redux";
import { PLAY_GAME, SET_OPACITY } from "./redux/consants/xucXacContant";

class KetQua extends Component {
  handleClickPlayGame = () => {
    this.props.handleSetXucXacOpacity(0);
    setTimeout(() => {
      this.props.handlePlayGame();
    }, 1000);
  };
  render() {
    let { luaChon, soBanThang, soLuotChoi } = this.props.xucXacState;
    return (
      <div className="">
        {luaChon && (
          <>
            <button
              style={{
                fontSize: 40,
              }}
              className="btn btn-warning"
              onClick={this.handleClickPlayGame}
            >
              Play Game
            </button>
            <p className="display-4 text-warning">Bạn chọn :{luaChon}</p>
          </>
        )}
        <p className="display-4 text-white">Số bàn thắng {soBanThang}</p>
        <p className="display-4 text-primary">Số lượt chơi {soLuotChoi}</p>
      </div>
    );
  }
}

let mapDispatchToProps = (dispatch) => {
  return {
    handlePlayGame: () => {
      dispatch({ type: PLAY_GAME });
    },
    handleSetXucXacOpacity: (opacity) => {
      dispatch({ type: SET_OPACITY, payload: opacity });
    },
  };
};

let mapStateToProps = (state) => {
  return {
    xucXacState: state.xucXacReducer,
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(KetQua);

// let alice = {
//   name: "Alice Nguyễn",
//   speak: function () {
//     console.log("print");
//     console.log(this.name);
//   },
// };

// alice.speak();

// let { speak } = alice;
// speak();

// let talk = alice.speak;
// console.log("speak: ", talk());

// count=3
// setInterval(() => {

// }, interval);

// clearInterval
