import React, { Component } from "react";
import { connect } from "react-redux";
import { CHOOSE_OPTION } from "./redux/consants/xucXacContant";
import { TAI, XIU } from "./xucXacUtils";

let cssBtn = {
  width: 250,
  height: 250,
  fontSize: 30,
  margin: "0px 100px",
};

class XucXac extends Component {
  renderMangXucXac = () => {
    return this.props.mangXucXac.map((item) => {
      return (
        <img
          style={{
            height: 100,
            margin: 20,
            opacity: this.props.xucXacOpacity,
            transition: "0.3s",
          }}
          src={item.img}
          alt=""
        />
      );
    });
  };
  render() {
    console.log(this.props.mangXucXac);
    return (
      <div className="pt-5">
        <button
          onClick={() => {
            this.props.handleLuaChon(TAI);
          }}
          style={cssBtn}
          className="btn btn-danger"
        >
          Tài
        </button>

        {this.renderMangXucXac()}
        <button
          onClick={() => {
            this.props.handleLuaChon(XIU);
          }}
          style={cssBtn}
          className="btn btn-secondary"
        >
          Xỉu
        </button>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    mangXucXac: state.xucXacReducer.mangXucXac,
    xucXacOpacity: state.xucXacReducer.xucXacOpacity,
  };
};
let mapDispatchToProps = (dispatch) => {
  return {
    handleLuaChon: (luaChon) => {
      dispatch({
        type: CHOOSE_OPTION,
        payload: luaChon,
      });
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(XucXac);
