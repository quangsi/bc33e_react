import { TAI, XIU } from "../../xucXacUtils";
import {
  CHOOSE_OPTION,
  PLAY_GAME,
  SET_OPACITY,
} from "../consants/xucXacContant";

let initialState = {
  mangXucXac: [
    {
      img: "./imgXucXac/1.png",
      giaTri: 1,
    },
    {
      img: "./imgXucXac/1.png",
      giaTri: 1,
    },
    {
      img: "./imgXucXac/1.png",
      giaTri: 1,
    },
  ],
  luaChon: null,
  soBanThang: 0,
  soLuotChoi: 0,
  xucXacOpacity: 1,
};

export const xucXacReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case PLAY_GAME: {
      let tongDiem = 0;
      let newMangXucXac = state.mangXucXac.map((item) => {
        let random = Math.floor(Math.random() * 6 + 1);
        tongDiem += random;
        return {
          img: `./imgXucXac/${random}.png`,
          giaTri: random,
        };
      });
      let ketQua = tongDiem >= 11 ? TAI : XIU;

      // if (ketQua == luaChon) {
      //   soBanThang++;
      // }
      ketQua == state.luaChon && state.soBanThang++;
      state.soLuotChoi++;

      state.mangXucXac = newMangXucXac;
      state.xucXacOpacity = 1;

      return { ...state };
    }
    case CHOOSE_OPTION: {
      state.luaChon = payload;
      return { ...state };
    }
    case SET_OPACITY: {
      return { ...state, xucXacOpacity: payload };
    }

    default:
      return state;
  }
};

// 1 6
// 0.111 * 7;
//  xỉu < 11 , tài >= 11
