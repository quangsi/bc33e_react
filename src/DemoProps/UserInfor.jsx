import React, { Component } from "react";

export default class UserInfor extends Component {
  render() {
    console.log(this.props);
    return (
      <div style={{ border: "2px solid blue" }} className="bg-warning ">
        <p>UserInfor</p>
        <p> Username:{this.props.user.name}</p>
        <br />

        <button onClick={this.props.handleClick} className="btn btn-danger">
          Change name to bob
        </button>

        <h2>{this.props.title}</h2>
      </div>
    );
  }
}

var a = 2;

var b = a;

var c = b;
