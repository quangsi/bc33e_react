import React, { Component } from "react";

export default class DemoState extends Component {
  // state : nơi lưu trữ dự liệu liên quan đến component ~ thay đổi dữ liệu này thì cần tạo giao diện mới tương ứng
  state = {
    like: 0,
    userInfor: {
      name: "alice",
    },
    share: 10,
  };

  handlePlusLike = () => {
    // setState dùng để update giá trị của state
    // setState là update ,không phải replace
    // this.state.like = this.state.like + 1;
    this.setState({
      like: this.state.like + 1,
    });
  };
  handleChagneName = () => {
    // this.state.userInfor.name = "Tom";
    this.setState({
      userInfor: {
        name: "Bob",
      },
    });
  };

  // state thay đổi dẫn theo render chạy lại
  render() {
    // setState tại đây => render vô tận
    console.log("yes render");
    return (
      <div>
        <p>{this.state.like}</p>

        <button onClick={this.handlePlusLike} className="btn btn-primary">
          Plus like
        </button>

        <br />

        <p className="">User name: {this.state.userInfor.name}</p>
        {/* handleChangeName */}
        <button onClick={this.handleChagneName} className="btn btn-warning">
          Change Alice To Bob
        </button>
      </div>
    );
  }
}
