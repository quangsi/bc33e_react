import React, { Component } from "react";

export default class EventBinding extends Component {
  // function không có tham số
  handleSayHello = () => {
    //

    console.log("Hello");
  };

  handleSayHelloByName = (name) => {
    console.log("Hello " + name);
  };

  // handleClick = () => {
  //   this.handleSayHelloByName("alice");
  // };

  render() {
    // here
    return (
      <div>
        <button onClick={this.handleSayHello} className="btn btn-success">
          Say Hello
        </button>
        <br />
        <button
          onClick={() => {
            this.handleSayHelloByName("Alice");
          }}
          // onClick={this.handleClick}
          className="btn btn-primary"
        >
          Say hello to Alice
        </button>
      </div>
    );
  }
}
// npm start
