import React from "react";
import { Component } from "react";

export default class DanhSachNguoiDung extends Component {
  renderTbody = () => {
    return this.props.dsnd.map((user) => {
      return (
        <tr key={user.id}>
          <td>{user.id}</td>
          <td>{user.name}</td>
          <td>{user.email}</td>
          <td>
            <button
              onClick={() => {
                this.props.handleEditUser(user);
              }}
              className="btn btn-success"
            >
              Sửa
            </button>
            <button
              onClick={() => {
                this.props.handleRemoveUser(user.id);
              }}
              className="btn btn-danger"
            >
              Xoá
            </button>
          </td>
        </tr>
      );
    });
  };
  render() {
    return (
      <div className="mt-5">
        <table className="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Email</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>{this.renderTbody()}</tbody>
        </table>
      </div>
    );
  }
}
