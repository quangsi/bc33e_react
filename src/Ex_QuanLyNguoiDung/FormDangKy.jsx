import React, { Component } from "react";
import { createRef } from "react";

export default class FormDangKy extends Component {
  constructor(props) {
    super(props);
  }
  state = {
    userInfor: {
      id: "",
      name: "",
      email: "",
    },
  };
  handleChangeInput = (e) => {
    console.log(e.target.name);
    let { value, name } = e.target;
    // c1
    this.setState({
      userInfor: { ...this.state.userInfor, [name]: value },
    });
    // c2
    // let cloneUserInfor = { ...this.state.userInfor };
    // cloneUserInfor[name] = value;
    // this.setState({ userInfor: cloneUserInfor });
  };

  handleSubmit = () => {
    // reset form
    this.setState({
      userInfor: {
        id: "",
        name: "",
        email: "",
      },
    });
    this.props.handleAddUser(this.state.userInfor);
  };
  UNSAFE_componentWillReceiveProps(nextProps) {
    console.log("data props mới", nextProps);
    console.log("this.props    ", this.props);
    if (nextProps.userEdit) {
      this.setState({
        userInfor: nextProps.userEdit,
      });
    }
  }
  render() {
    return (
      <div>
        <form>
          {/* start input */}
          <div className="form-group">
            <input
              onChange={this.handleChangeInput}
              value={this.state.userInfor.id}
              type="text"
              className="form-control"
              name="id"
              placeholder="Id"
            />
          </div>
          <div className="form-group">
            <input
              onChange={this.handleChangeInput}
              value={this.state.userInfor.name}
              type="text"
              className="form-control"
              name="name"
              placeholder="Name"
            />
          </div>
          <div className="form-group">
            <input
              onChange={this.handleChangeInput}
              value={this.state.userInfor.email}
              type="text"
              className="form-control"
              name="email"
              placeholder="Email"
            />
          </div>
          {/* end  input*/}

          <button
            type="button"
            onClick={this.handleSubmit}
            className="btn btn-warning"
          >
            Add user
          </button>

          <button
            type="button"
            onClick={() => {
              this.props.handleUpdateUser(this.state.userInfor);
            }}
            className="btn btn-secondary"
          >
            Update user
          </button>
        </form>
      </div>
    );
  }
}

let alice = {
  age: 2,
};
alice.age = 4;

alice["age"] = 4;

let key = "age";
alice[key] = 4;
