import React from "react";
import { Component } from "react";
import DanhSachNguoiDung from "./DanhSachNguoiDung";
import FormDangKy from "./FormDangKy";

export default class Ex_QuanLyNguoiDung extends Component {
  state = {
    dsnd: [],
    userEdit: null,
  };
  handleEditUser = (user) => {
    this.setState({
      userEdit: user,
    });
  };
  handleUpdateUser = (updateUser) => {
    let index = this.state.dsnd.findIndex((user) => {
      return user.id == updateUser.id;
    });
    if (index !== -1) {
      let cloneDssd = [...this.state.dsnd];
      cloneDssd[index] = updateUser;
      this.setState({
        dsnd: cloneDssd,
        userEdit: null,
      });
    }
  };

  handleAddUser = (user) => {
    var cloneDsnd = [...this.state.dsnd, user];
    this.setState(
      {
        dsnd: cloneDsnd,
      },
      () => {
        console.log(this.state.dsnd);
      }
    );
  };
  handleRemoveUser = (idUser) => {
    let newDsnd = this.state.dsnd.filter((user) => {
      return user.id !== idUser;
    });

    this.setState({ dsnd: newDsnd });
  };

  render() {
    return (
      <div className="container py-5">
        <FormDangKy
          handleUpdateUser={this.handleUpdateUser}
          userEdit={this.state.userEdit}
          handleAddUser={this.handleAddUser}
        />
        <DanhSachNguoiDung
          handleEditUser={this.handleEditUser}
          handleRemoveUser={this.handleRemoveUser}
          dsnd={this.state.dsnd}
        />
      </div>
    );
  }
}
