import { GIAM_SO_LUONG, TANG_SO_LUONG } from "../constants/numberConstant";

let initialState = {
  number: 10,
};

export const numberReducer = (state = initialState, action) => {
  switch (action.type) {
    case TANG_SO_LUONG: {
      state.number = state.number + action.payload;
      return { ...state };
    }
    case GIAM_SO_LUONG: {
      state.number -= action.payload;
      return { ...state };
    }
    default:
      return state;
  }
};

// redux shallow compare

// {...state} : từ state , clone ra 1 state mới

// {...state, key:value} : clone object và udpate key, value

// [...array, item] : clone array và bổ sung thêm item

// let user1 = {
//   name: "alice",
// };

// let user2 = {...user1};

// user2.name = "Bob";

// user1.name;
