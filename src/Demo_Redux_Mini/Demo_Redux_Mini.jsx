import React, { Component } from "react";
import { connect } from "react-redux";
import {
  giamSoLuongAction,
  tangSoLuongAction,
} from "./redux/actions/numberAction";
import { GIAM_SO_LUONG, TANG_SO_LUONG } from "./redux/constants/numberConstant";

class Demo_Redux_Mini extends Component {
  render() {
    return (
      <div>
        <button
          onClick={() => {
            this.props.giamSoLuong(10);
          }}
        >
          Giảm số lượng
        </button>
        <span className="display-4 text-success">{this.props.soLuong}</span>

        <button onClick={this.props.tangSoluong} className="btn btn-success">
          Tăng số lượng
        </button>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  //  1 key ~  1 props
  return {
    soLuong: state.numberReducer.number,
  };
};

let mapDispatchToProps = (dispatch) => {
  //  1 key ~  1 props
  return {
    tangSoluong: () => {
      dispatch(tangSoLuongAction());
    },
    giamSoLuong: (soLuong) => {
      dispatch(giamSoLuongAction(soLuong));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Demo_Redux_Mini);

// alias import
