import React, { Component } from "react";

export default class ConditionalRendering extends Component {
  isLogin = false;

  handleLogin = () => {
    this.isLogin = true;
    console.log(" this.isLogin: ", this.isLogin);
  };
  // handleLogout = () => {};
  renderContent = () => {
    if (this.isLogin) {
      return (
        <>
          <p>Đã đăng nhập</p>
          <button className="btn btn-danger">Đăng xuất</button>
        </>
      );
    } else {
      return (
        <>
          <p>Chưa đăng nhập</p>
          <button onClick={this.handleLogin} className="btn btn-primary">
            Đăng nhập
          </button>
        </>
      );
    }
  };

  render() {
    return (
      <div>
        {this.renderContent()}

        {/* support toán tử 3 ngôi */}
        {/* {this.isLogin ? "Đã đăng nhập" : "Chưa đăng nhập"} */}
      </div>
    );
  }
}
